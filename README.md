
# Proyecto Portafolio-CV JulioRomero Dev
Desarrollado utilizando unicamente HTML, CSS y algo de Javascript con el objetivo de mejorar las habilidades de maquetación y estilos, utilizando la filosofia de Mobile First utilizando CSS puro.

## Deploy en gitlab page utilizando pipelines
[Portafolio-CV julioRomero](https://julioale21.gitlab.io/portfolio-cv)
